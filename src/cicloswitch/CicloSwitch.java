/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cicloswitch;

/**
 *
 * @author ize
 */
public class CicloSwitch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Variables a ocupar
        int numero = 10;

        String numeroTexto = "Valor desconocido";

        switch (numero) {
            case 1:
                numeroTexto = "Numero uno";
                System.out.println("numeroTexto = " + numeroTexto);
                break;
            case 2:
                numeroTexto = "Numero dos";
                System.out.println("numeroTexto = " + numeroTexto);
                break;
            case 3:
                numeroTexto = "Numero tres";
                System.out.println("numeroTexto = " + numeroTexto);
                break;
            case 5:
                numeroTexto = "Numero cinco";
                System.out.println("numeroTexto = " + numeroTexto);
                break;
            default:
                numeroTexto = "No se encontro el caso";
                System.out.println("numeroTexto = " + numeroTexto);
        }

    }

}
